package com.orsyp.audit.datamodel.statsobjects;

import java.io.Serializable;
import java.util.Vector;

public class UniverseLog implements Serializable{
	private static final long serialVersionUID = 1L;

	public boolean present;
	public String fileSize;
	public Vector<String[]> recurringErrors = new Vector<String[]>();
	
	public Vector<String[]> recurringWarnings = new Vector<String[]>();

}
