package com.orsyp.audit.datamodel.statsobjects;

import com.orsyp.audit.datamodel.AuditDataModel;

public abstract class GlobalStat extends Stat {
	private static final long serialVersionUID = 1L;

	public GlobalStat() {}
	
	public GlobalStat(AuditDataModel auditData) {
		build(auditData);
	}
	
	public abstract void build(AuditDataModel auditData); 
}
