package com.orsyp.audit.datamodel.statsobjects;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class FileBrowserItem implements Comparator<FileBrowserItem>{
	
	public String fullPath = null;
	public String name;
	public long size;
	public long compSize;
	public long modified;
	public boolean dir = false;
	public boolean zip = false;
	
	public ArrayList<FileBrowserItem> children = new ArrayList<FileBrowserItem>();
	
	public enum Sorting {NAME_ASC, NAME_DESC, SIZE_ASC, SIZE_DESC, DATE_ASC, DATE_DESC};	
	public static Sorting sorting = Sorting.NAME_ASC;
	

	public FileBrowserItem(File f) {
		if (f==null)
			return;
		fullPath = f.getAbsolutePath(); 
		name = f.getName();
		size = f.length();
		dir = f.isDirectory();
		zip = name.toLowerCase().endsWith(".zip") || 
			  name.toLowerCase().endsWith(".tgz")  ||  
			  name.toLowerCase().endsWith(".gz")  ||
			  name.toLowerCase().endsWith(".tar")  ||
			  name.toLowerCase().endsWith(".z");
		modified = f.lastModified();
	}


	@Override
	public int compare(FileBrowserItem f1, FileBrowserItem f2) {
		long size1 = f1.size;
		long size2 = f2.size;
		if (f1.dir)
			size1=0;
		if (f2.dir)
			size2=0;
		switch (sorting) {
			case SIZE_ASC: 	return Long.valueOf(size1).compareTo(size2);
			case DATE_ASC:	return Long.valueOf(f1.modified).compareTo(f2.modified);
			case NAME_DESC: return - f1.name.toUpperCase().compareTo(f2.name.toUpperCase());
			case SIZE_DESC: return - Long.valueOf(size1).compareTo(size2);
			case DATE_DESC: return - Long.valueOf(f1.modified).compareTo(f2.modified);
				
		}
		return f1.name.toUpperCase().compareTo(f2.name.toUpperCase());
	}


	public String getSizeStr() {	
		/*if (size > 1024*1024*1024)
			return String.valueOf(size / 1024 /1024/1024) + " Gb";
		
		if (size > 1024*1024)
			return String.valueOf(size / 1024 /1024) + " Mb";
		
		if (size > 1024)
			return String.valueOf(size / 1024) + " Kb";
		*/
		return String.format("%,d", size);
	}

	public String getModifiedStr() {
		return new SimpleDateFormat("dd/MM/yy HH:mm").format(modified) ;
	}


	public String getCompSizeStr() {
		if (compSize>0)
			return String.valueOf(compSize);
		return "";
	}


	public void sortChildren() {
		Collections.sort(children, this);
		for (FileBrowserItem fi : children)
			fi.sortChildren();			
	}

}
