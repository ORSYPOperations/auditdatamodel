package com.orsyp.audit.datamodel.statsobjects;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.orsyp.audit.datamodel.AuditWarning;

public abstract class Stat implements Serializable {
	private static final long serialVersionUID = 1L;
	
	protected String title;
	protected String htmlText;
	
	protected String[] exportHeader;
	protected String[][] exportTable;
	protected int[] exportAttributes;
	
	
	protected List<AuditWarning> warnings = new ArrayList<AuditWarning>();	
	public List<AuditWarning> getWarnings() {
		return warnings;
	}
	public void addWarning(AuditWarning w) {
		warnings.add(w);
	}	
	
	protected boolean unavailable = false;
	
	protected boolean notPresent = false;
	
	
	public String getHtmlText() {
		return htmlText;
	}

	public String getTitle() {
		return title;
	}
	
	public String[] getExportHeader() {
		return exportHeader;
	}

	public String[][] getExportTable() {
		return exportTable;
	}
	
	public String[][] getExportHeaderAndTable() {
		String[][] table = new String[exportTable.length+1][exportHeader.length];
		table[0] = exportHeader;
		for (int i=1; i<=exportTable.length; i++) 
			table[i] = exportTable[i-1];
		return table;
	}
	

	public boolean hasWarnings() {
		return warnings.size()>0;
	}


	public boolean isUnavailable() {
		return unavailable;
	}
	
	public boolean isNotPresent() {
		return notPresent;
	}
}
