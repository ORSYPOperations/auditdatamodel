package com.orsyp.audit.datamodel.statsobjects;

import com.orsyp.audit.datamodel.NodeData;

public abstract class NodeStat extends Stat {
	private static final long serialVersionUID = 1L;
	
	public NodeStat() {}
	
	public NodeStat(NodeData nodeData) {
		build(nodeData);
	}

	public abstract void build(NodeData nodeData);
	
	
	protected boolean getAvailability(String file, NodeData nodeData) {
		if (nodeData.duVersion!=null)
			if (nodeData.duVersion.startsWith("5")) {
				if (file.equals("values.xml") ||
					file.equals("unienv.bat") || 
					file.equals("unistart.bat") ||
					file.equals("unistop.bat") ||
					file.equals("unienv.ksh") || 
					file.equals("unistart.ksh") ||
					file.equals("unistop.ksh") ||
					file.equals("unienv") || 
					file.equals("unistart") ||
					file.equals("unistop"))
					return false;
			}
			else 
			if (nodeData.duVersion.startsWith("6")) {
				if (file.startsWith("uxsrsrv") ||
					file.startsWith("security") )
					return false;
			}
		return true;
	}
}
