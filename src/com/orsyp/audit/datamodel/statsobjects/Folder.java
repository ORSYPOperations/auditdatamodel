package com.orsyp.audit.datamodel.statsobjects;

import java.io.Serializable;
import java.util.Vector;

public class Folder implements Serializable{
	private static final long serialVersionUID = 1L;
	
	public String name;
	public Vector<Folder> subfolders = new Vector<Folder>();
	
	public String getTreeString() {
		String s = name;
		for (Folder f : subfolders) 
			s +="\n +-- " + f.getTreeString(); 		
		return s;
	}
	
}
