package com.orsyp.audit.datamodel.statsobjects;

public enum StatKeys {
	
		OVERVIEW, VERSIONS, AREAS, COMPANIES, MANAGERS, NODES,     
		APPLICATIONS, DOMAINS, MUS, MUTYPES, RULES, CLASSES,
		CALENDARS, CALENDARS_APP, CALENDARS_EXP,  CALENDARS_SIM, CALENDARS_INT,
		Users, UserProfiles, HDP, DQM, 
		Objects,
		UProcs, UProcs_APP, UProcs_EXP, UProcs_SIM, UProcs_INT, 
		Sessions, Sessions_APP, Sessions_EXP, Sessions_INT, Sessions_SIM, 
		Tasks, Tasks_APP, Tasks_EXP, Tasks_INT, Tasks_SIM, 
		Resources, Resources_APP, Resources_EXP, Resources_INT, Resources_SIM, 
		Scripts, FolderStructure, 
		uxsrsrv_sck, uxsrsrv_alias, U_ANTE_UPROC, U_POST_UPROC, uxsetenv, unienv, uxstartup, uxstartup_gen, uxshutdown, u_batch, security,
		values_xml, unistart, unistop,
		FileSystem, Ports, Environment, hosts, services, 
		Executions, Executions_APP, Executions_EXP, Executions_INT, Executions_SIM, 
		Log, Purge, Reorganization, 
		UPGRADE_Profiles, UPGRADE_Users   
}
