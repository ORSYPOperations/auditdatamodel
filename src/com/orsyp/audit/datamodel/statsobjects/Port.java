package com.orsyp.audit.datamodel.statsobjects;

import java.io.Serializable;

public class Port implements Serializable{
	private static final long serialVersionUID = 1L;
	
	public Port() {}
	
	public Port(String service, String portProtocol, String comment) {
		super();
		this.service = service;
		this.portProtocol = portProtocol;
		this.comment = comment;
	}
	
	public String service;
	public String portProtocol;
	public String comment;

}
