package com.orsyp.audit.datamodel.statsobjects;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

public class DuCommands implements Serializable{
	private static final long serialVersionUID = 1L;
	
	//TODO more commands
	public static List<String> commands = Arrays.asList(new String [] {"uxadd" , "uxdel", "uxlst", "uxordre", "uxset", "uxshw", "uxupd"});
}
