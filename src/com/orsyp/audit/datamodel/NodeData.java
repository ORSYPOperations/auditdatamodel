package com.orsyp.audit.datamodel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.Vector;

import com.orsyp.audit.datamodel.duobjects.Application;
import com.orsyp.audit.datamodel.duobjects.DQM;
import com.orsyp.audit.datamodel.duobjects.Domain;
import com.orsyp.audit.datamodel.duobjects.HDP;
import com.orsyp.audit.datamodel.duobjects.MU;
import com.orsyp.audit.datamodel.duobjects.MUType;
import com.orsyp.audit.datamodel.duobjects.Node;
import com.orsyp.audit.datamodel.duobjects.Rule;
import com.orsyp.audit.datamodel.duobjects.UprClass;
import com.orsyp.audit.datamodel.duobjects.User;
import com.orsyp.audit.datamodel.statsobjects.FileBrowserItem;
import com.orsyp.audit.datamodel.statsobjects.Port;
import com.orsyp.audit.datamodel.statsobjects.Stat;
import com.orsyp.audit.datamodel.statsobjects.StatKeys;
import com.orsyp.audit.datamodel.statsobjects.UniverseLog;



public class NodeData implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public boolean analyzePresent = false;
	public boolean envPresent = false;
	public boolean lstPresent = false;
	public boolean dufilesPresent = false;
	public boolean filesPresent = false;
	public boolean dqmPresent = false;
	public boolean sysPresent = false;
	public boolean dlsPresent = false;
	public boolean logPresent = false;
	public boolean patPresent = false;
	
	public Map<String,AreaData> areas = new TreeMap<String,AreaData>(String.CASE_INSENSITIVE_ORDER);
	
	public Map<String,MU> mus = new TreeMap<String,MU>(String.CASE_INSENSITIVE_ORDER);
	public Map<String,Rule> rules = new TreeMap<String,Rule>(String.CASE_INSENSITIVE_ORDER);
	public Map<String,User> users = new TreeMap<String,User>(String.CASE_INSENSITIVE_ORDER);
	
	public Map<String,Application> applications = new TreeMap<String,Application>(String.CASE_INSENSITIVE_ORDER);	
	public Map<String,UprClass> classes = new TreeMap<String,UprClass>(String.CASE_INSENSITIVE_ORDER);
	public Map<String,Domain> domains = new TreeMap<String,Domain>(String.CASE_INSENSITIVE_ORDER);
	public Map<String,MUType> mutypes = new TreeMap<String,MUType>(String.CASE_INSENSITIVE_ORDER);
	public Map<String,HDP> hdp = new TreeMap<String,HDP>(String.CASE_INSENSITIVE_ORDER);
	public Map<String,DQM> dqm = new TreeMap<String,DQM>(String.CASE_INSENSITIVE_ORDER);

	public String duVersion = "Unknown";
	public String duPatch = "";
	public String os = "Unknown";
	public String hostname = "Unknown";
	public String company = null;
	public String node = null;
	public boolean hiddenVars = false;
	public boolean proxy = false;
	public boolean clustered = false;
	public String purgeConfig = null;
	
	public NodeTag tag = NodeTag.UNKNOWN;

	public Map<String,String> managers = new TreeMap<String,String>(String.CASE_INSENSITIVE_ORDER);
	public Map<String,String> files = new TreeMap<String,String>(String.CASE_INSENSITIVE_ORDER);
	public Map<String,String> profiles = new TreeMap<String,String>(String.CASE_INSENSITIVE_ORDER);
	
	public Map<String,Node> nodes = new TreeMap<String,Node>(String.CASE_INSENSITIVE_ORDER);
	
	public Map<StatKeys,Stat> nodeStats = new HashMap<StatKeys,Stat>();
	
	public Vector<String> businessViews = new Vector<String>();
	public Vector<String> jobChains = new Vector<String>();
	
	public Map<String,String> environment = new TreeMap<String,String>(String.CASE_INSENSITIVE_ORDER);
	
	public boolean supervision = false;
	public UniverseLog universeLog = new UniverseLog();
	public Vector<Port> ports = new Vector<Port>();
	
	public TreeMap<String,Integer> usedDuCommands = new TreeMap<String,Integer>(); //cmd name, times used
	public int reorganizations = 0;
	
	public FileBrowserItem fileBrowserRoot;
	public String fileSystemInfo;	
	
	//----------------
	
	private List<AuditWarning> warnings = new ArrayList<AuditWarning>();
	
	public List<AuditWarning> getWarnings() {
		return warnings;
	}

	public void addWarning(AuditWarning w) {
		warnings.add(w);
	}

}
