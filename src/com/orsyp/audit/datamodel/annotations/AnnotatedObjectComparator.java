package com.orsyp.audit.datamodel.annotations;

import java.util.Comparator;

/**
 * Compare annotated objects
 * @author rbr
 *
 */
public class AnnotatedObjectComparator implements Comparator<Object> {

	private String fieldName;
	private boolean ascending;
	
	/**
	 * Initialize comparator with annotated field name and sort order
	 * @param fieldName
	 * @param sortAsc
	 */
	public AnnotatedObjectComparator(String fieldName, boolean sortAsc) {
		this.fieldName = fieldName;
		ascending = sortAsc;
	}

	@Override
	public int compare(Object obj1, Object obj2) {
		String s1 = AnnotatedObjUtils.getFieldValue(obj1, fieldName);
		String s2 = AnnotatedObjUtils.getFieldValue(obj2, fieldName);
		int result = 0;
		
		if (s1==null)
			return 1;

		if (s2==null)
			return -1;
		
		result = s1.compareToIgnoreCase(s2);		
		if (!ascending)
			result = - result;		
		return result;
	}
	
}
