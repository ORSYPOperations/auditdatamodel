package com.orsyp.audit.datamodel.annotations;

import java.io.Serializable;

/**
 * Field descriptor object
 * @author rbr
 *
 */
public class FieldDesc implements Comparable<FieldDesc>, Serializable{
	private static final long serialVersionUID = 1L;
	
	public String fieldId;
	public String displayName;
	public boolean isMetadata = false;
	public boolean isEditable = false;
	
	
	@SuppressWarnings("unused")
	private FieldDesc() {}
	
	
	public FieldDesc(String fieldId, String displayName) {
		super();
		this.fieldId = fieldId;
		this.displayName = displayName;
	}


	public FieldDesc(String fieldId, String displayName, boolean isMetadata, boolean isEditable) {
		super();
		this.fieldId = fieldId;
		this.displayName = displayName;
		if ((this.displayName==null) || (this.displayName.length()==0))
			this.displayName = fieldId;
		this.isMetadata = isMetadata;
		this.isEditable = isEditable;
	}


	@Override
	public int compareTo(FieldDesc f) {	
		//group metadata fields at the end when sorting
		int v = new Boolean(isMetadata).compareTo(new Boolean(f.isMetadata));
		if (v==0)			
			return displayName.compareToIgnoreCase(f.displayName);
		else
			return v;
	}	
	
	

}
