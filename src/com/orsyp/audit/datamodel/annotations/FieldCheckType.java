package com.orsyp.audit.datamodel.annotations;

public enum FieldCheckType {
	DU_STRING,
	INTEGER, 
	FREE, 
	BOOLEAN,
	RETCODE
}
