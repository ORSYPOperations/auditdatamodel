package com.orsyp.audit.datamodel.annotations;

/**
 * Enumeration of allowed field types in ListViewField annotation   
 * @author rbr
 */

public enum ListViewFieldType {
	STRING,
	STRING_LIST,
	TYPE,
	LISTABLEOBJ,
	LISTABLEOBJ_LIST
}
