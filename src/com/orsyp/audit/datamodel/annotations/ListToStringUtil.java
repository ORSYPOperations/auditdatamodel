package com.orsyp.audit.datamodel.annotations;

import java.io.Serializable;
import java.util.List;

/**
 * Utility method to get a string representation of a list of objects implementing ListableObj interface
 * 
 * @author rbr
 *
 */
public class ListToStringUtil implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * Get a descriptive string of a list of objects 
	 * 
	 * @param list List of objects implementing ListableObj interface
	 * @return string representation of the list
	 */
	public static String listToString(List<ListableObj> list) {
		String ret = "[";
		for (ListableObj lobj : list) {
			if (ret.length()>1)
				ret +=",";
			ret += lobj.getListString();
		}
		return ret + "]";
	};
	
	@SuppressWarnings("rawtypes")
	public static String listToStringGen(List list) {
		String ret = "[";
		for (Object o : list) 
			if (o instanceof ListableObj) {
				ListableObj l = (ListableObj)o;
				if (ret.length()>1)
					ret +=",";
				ret += l.getListString();
			}
		return ret + "]";
	};

	
	public static String arrayToString(Object list[]) {
		String ret = "[";
		for (int i = 0; i< list.length; i++) {
			if (ret.length()>1)
				ret +=",";
			ret += ((ListableObj)list[i]).getListString();
		}
		return ret + "]";
	};
}
