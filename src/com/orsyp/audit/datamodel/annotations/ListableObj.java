package com.orsyp.audit.datamodel.annotations;

public interface ListableObj {

	public String getListString();
	
}
