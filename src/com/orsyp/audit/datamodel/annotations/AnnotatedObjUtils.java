package com.orsyp.audit.datamodel.annotations;

import java.lang.reflect.Field;
import java.util.List;

/**
 * Utils for annotated objects
 * @author rbr
 *
 */
public class AnnotatedObjUtils {

	/**
	 * Get value of an object field that has a "ListViewField" annotation
	 * 
	 * @param obj
	 * @param field
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static String getFieldValue(Object obj, String field) {
		Class<? extends Object> c = obj.getClass();		
		try {
			Field f = c.getField(field);
			if (f.isAnnotationPresent(ListViewField.class)) {
				ListViewField ann = f.getAnnotation(ListViewField.class);
				String str = null;
				switch (ann.type()) {
					case STRING: 											
					case TYPE: 			str = c.getField(field).get(obj).toString(); break;
					case STRING_LIST:	str = ((List<String>) c.getField(field).get(obj)).toString(); break;
					case LISTABLEOBJ:	str = ((ListableObj) c.getField(field).get(obj)).getListString(); break;
					case LISTABLEOBJ_LIST:	str = ListToStringUtil.listToString(((List<ListableObj>) c.getField(field).get(obj))); break;
				}				
				
				if (str.startsWith("[") && str.endsWith("]"))
					str = str.substring(1,str.length()-1);
				return str;
			}
		} catch (Exception e) {}
		return null;
	}


}
