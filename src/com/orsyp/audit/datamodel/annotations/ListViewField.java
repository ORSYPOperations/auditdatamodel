package com.orsyp.audit.datamodel.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.annotation.ElementType;

/**
 * This annotation is used to tag fields of classes that are visualized in the gui listview.
 * Column title and width can be set.
 * Cell rendering will show strings according to "type".
 * Editing can be disabled and a custom comparator can be set, the default is a string comparator 
 * 
 * @author rbr
 *
 */

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)

public @interface ListViewField {
	
	public String title() default "";	
	public ListViewFieldType type() default ListViewFieldType.STRING;	
	public boolean editable() default false; 
	public String comparator() default "";
	public int width() default 70;
	public FieldCheckType check() default FieldCheckType.DU_STRING;

}
