package com.orsyp.audit.datamodel.utils;

import java.util.Arrays;


public class StringUtils {

	public static String[] objectsToStringArray(Object objs[]) {
		return Arrays.copyOf(objs, objs.length, String[].class);
	}
	
	public static String exceptionToLogString(Exception e) {
		String[] tk = e.getClass().toString().split("\\.");
		String s = tk[tk.length-1] + " at:";
		String stStr = "";
		for (StackTraceElement st : e.getStackTrace()) {
			if (stStr.length()==0) {
				stStr = st.getMethodName() + "("+st.getFileName() + ":" + st.getLineNumber() + ")";
				if (st.getClassName().startsWith("com.orsyp.migration")) 
					break;
			}
			
			if (st.getClassName().startsWith("com.orsyp.migration")) {
				if (stStr.length()>0)
					stStr += " ... ";
				stStr += st.getMethodName() + "("+st.getFileName() + ":" + st.getLineNumber() + ")";
				break;
			}
		}
		
		if (e.getMessage()!=null && e.getMessage().length()>0)
			stStr += " - " + e.getMessage(); 
		return s + " " + stStr;
	}

	public static String fileLengthString(int size) {
		if (size>=1024*1024*1024)
			return String.valueOf(size/1024/1024/1024)+" GB";
		if (size>=1024*1024)
			return String.valueOf(size/1024/1024)+" MB";
		if (size>=1024)
			return String.valueOf(size/1024)+" KB";
		return String.valueOf(size)+" B";
	}
	
}
