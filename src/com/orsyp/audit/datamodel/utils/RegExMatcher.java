package com.orsyp.audit.datamodel.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Regular expression utils
 * @author rbr
 *
 */
public class RegExMatcher {

	public static List<String> getMatches(String str, String regExp) {
		List<String> res = new ArrayList<String>();
		Pattern expr = Pattern.compile(regExp);
		Matcher matcher = expr.matcher(str);
		while ( matcher.find() )                 
			res.add(matcher.group());
		return res;
	}
	
	public static String getFirstMatch(String str, String regExp) {
		Pattern expr = Pattern.compile(regExp);
		Matcher matcher = expr.matcher(str);
		while ( matcher.find() )                 
			return matcher.group();
		return null;
	}

}
