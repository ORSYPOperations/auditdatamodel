package com.orsyp.audit.datamodel;

import java.io.Serializable;
import java.util.Map;
import java.util.TreeMap;
import java.util.Vector;

import com.orsyp.audit.datamodel.duobjects.Calendar;
import com.orsyp.audit.datamodel.duobjects.Execution;
import com.orsyp.audit.datamodel.duobjects.Resource;
import com.orsyp.audit.datamodel.duobjects.Session;
import com.orsyp.audit.datamodel.duobjects.Task;
import com.orsyp.audit.datamodel.duobjects.UProc;
import com.orsyp.audit.datamodel.duobjects.lightobjects.ResourceLt;
import com.orsyp.audit.datamodel.duobjects.lightobjects.SessionLt;
import com.orsyp.audit.datamodel.duobjects.lightobjects.TaskLt;
import com.orsyp.audit.datamodel.duobjects.lightobjects.UProcLt;

public class AreaData implements Serializable {
	private static final long serialVersionUID = 1L;

	public String area;
	
	public AreaData() {
	}
	
	public AreaData(String area) {
		this.area = area;
	}
	
	public Map<String,Session> sessions = new TreeMap<String,Session>(String.CASE_INSENSITIVE_ORDER);
	public Map<String,Resource> resources = new TreeMap<String,Resource>(String.CASE_INSENSITIVE_ORDER);
	public Map<String,UProc> uprocs = new TreeMap<String,UProc>(String.CASE_INSENSITIVE_ORDER);
	public Map<String,Task> tasks = new TreeMap<String,Task>(String.CASE_INSENSITIVE_ORDER);
	
	public Map<String,SessionLt> sessionsLt = new TreeMap<String,SessionLt>(String.CASE_INSENSITIVE_ORDER);
	public Map<String,ResourceLt> resourcesLt = new TreeMap<String,ResourceLt>(String.CASE_INSENSITIVE_ORDER);
	public Map<String,UProcLt> uprocsLt = new TreeMap<String,UProcLt>(String.CASE_INSENSITIVE_ORDER);
	public Map<String,TaskLt> tasksLt = new TreeMap<String,TaskLt>(String.CASE_INSENSITIVE_ORDER);
	
	public Vector<Calendar> calendars = new Vector<Calendar>();
	public Vector<Execution> executions = new Vector<Execution>();
	
	public boolean tooManyExecution = false;
		
}
