package com.orsyp.audit.datamodel;

import java.io.Serializable;

import com.orsyp.audit.datamodel.duobjects.Resource;
import com.orsyp.audit.datamodel.duobjects.Session;
import com.orsyp.audit.datamodel.duobjects.Task;
import com.orsyp.audit.datamodel.duobjects.UProc;
import com.orsyp.audit.datamodel.statsobjects.StatKeys;

public class AuditWarning implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public enum Type {ERROR,WARNING,INFO}; 
	public enum ObjectType {ENTITY,SESSION,UPROC,RESOURCE,TASK,IGNORED};
	public enum Category {UNKNOWN, MISSING_FILES,UXTRACE_LIMITATION, UNUSED_OBJECT, 
							MISSING_OBJECT, MISSING_FIELD, DU_COMMAND, CONFLICTING_DEFS,
							BAD_RESOURCE_USAGE, CUSTOMIZED_FILES};
	
	public AuditWarning(String text, Type type, StatKeys[] keys) {
		super();
		this.text = text;
		this.type = type;
		this.keys = keys;
	}
	
	public AuditWarning(String text, Type type, Category cat, StatKeys[] keys) {
		super();
		this.text = text;
		this.type = type;
		this.category = cat;
		this.keys = keys;
	}

	public AuditWarning(BaseDuObject o, String text, Type type, StatKeys[] keys) {
		super();
		init(o,text,type,keys);
	}
	
	public AuditWarning(BaseDuObject o, String text, Type type, Category cat, StatKeys[] keys) {
		super();
		init(o,text,type,keys);
		this.category = cat;
	}
	
	private void init(BaseDuObject o, String text, Type type, StatKeys[] keys){
		if (o!=null) {
			this.objectId = o.name;
			if (o instanceof UProc)
				this.objectType = ObjectType.UPROC;
			else
				if (o instanceof Task)
					this.objectType = ObjectType.TASK;
				else
					if (o instanceof Session)
						this.objectType = ObjectType.SESSION;
					else
						if (o instanceof Resource)
							this.objectType = ObjectType.RESOURCE;
		}
		this.text = text;
		this.type = type;
		this.keys = keys;
	}
	

	@SuppressWarnings("unused")
	private AuditWarning() {
		super();
	}


	public String objectId = null;
	public String text;
	public Type type = Type.INFO;
	public ObjectType objectType = ObjectType.IGNORED;  
	public Category category = Category.UNKNOWN;
	public StatKeys[] keys = null;

	/**
	 * Show the warning as HTML with browsable object links
	 * @return
	 */
	public String getHtmlText() {
		if (objectId!=null) {
			return String.format("<pre id=\"%s\">%-7s <a href=\"%s:%s\">%s</a> %s</pre>\n",									
									type.toString().toLowerCase().charAt(0),
									type.toString(),
									objectType.toString(),
									objectId, //link
									objectId,
									text);
		}
		else
			return String.format("<pre id=\"%s\">%-7s %s</pre>\n",
					type.toString().toLowerCase().charAt(0),
					type.toString(),
					text);
	}

	public static String getCategoryName(Category cat) {
		switch (cat) {
			case MISSING_FILES: 	return "Missing files";
			case UXTRACE_LIMITATION:return "UxTrace limitations";
			case UNUSED_OBJECT:  	return "Unused objects";
			case MISSING_OBJECT:	return "Missing objects";
			case DU_COMMAND:		return "$U commands in UProcs";
			case MISSING_FIELD:		return "Missing fields";
			case CONFLICTING_DEFS:	return "Conflicting object definitions";
			case BAD_RESOURCE_USAGE:return "Bad logical resource usage";
			case CUSTOMIZED_FILES:  return "Customized files";
		}
		return "Other warnings";
	}

}
