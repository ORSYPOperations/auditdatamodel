package com.orsyp.audit.datamodel.extensions;

import java.io.Serializable;

public class AuditScript  implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public String id;
	public String title;
	public String description;
	public String code;


}