package com.orsyp.audit.datamodel;

public enum NodeTag {
	UNKNOWN, PRODUCTION, TEST
}
