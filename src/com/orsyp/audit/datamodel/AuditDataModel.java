package com.orsyp.audit.datamodel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.List;
import java.util.TreeMap;

import com.orsyp.audit.datamodel.statsobjects.Stat;
import com.orsyp.audit.datamodel.statsobjects.StatKeys;

public class AuditDataModel implements Serializable {

	private static final long serialVersionUID = 1L;
	
	public Map<String,NodeData> nodeAuditData = new TreeMap<String,NodeData>(String.CASE_INSENSITIVE_ORDER);
	
	public Map<StatKeys,Stat> globalStats = new HashMap<StatKeys,Stat>();
	
	private List<AuditWarning> warnings = new ArrayList<AuditWarning>();
	
	public List<AuditWarning> getWarnings() {
		return warnings;
	}

	public void addWarning(AuditWarning w) {
		warnings.add(w);
	}
	
	public void clearWarnings() {
		warnings.clear();
	}
}
