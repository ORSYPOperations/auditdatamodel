package com.orsyp.audit.datamodel;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.TreeMap;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;
import com.orsyp.audit.datamodel.duobjects.Resource;
import com.orsyp.audit.datamodel.duobjects.Session;
import com.orsyp.audit.datamodel.duobjects.Task;
import com.orsyp.audit.datamodel.duobjects.UProc;
import com.orsyp.audit.datamodel.extensions.AuditExtensionCode;
import com.orsyp.audit.datamodel.extensions.AuditScript;
import com.orsyp.audit.datamodel.filter.DataFilter;
import com.orsyp.audit.datamodel.scripts.ScriptExecution;
import com.orsyp.audit.datamodel.annotations.ListViewField;
import com.orsyp.audit.datamodel.annotations.FieldDesc;

public class AuditProject implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public AuditDataModel auditDataModel = null;
	public NodeData currentNodeModel = null;
	public AreaData currentArea = null;
	
	public String name;
	
	public String host = "localhost"; 	//test vmstlrhsup5
	public int port = 4184;
	public String uvmsUser = "admin";	//test univa
	public String uvmsPassword;			//test univa

	public List<DataFilter> filters = new ArrayList<DataFilter>();
	
	private HashMap<String,FieldDesc> fieldList = null;

	public ArrayList<AuditExtensionCode> extensions = new ArrayList<AuditExtensionCode>();
	public ArrayList<AuditScript> scripts = new ArrayList<AuditScript>();
	public HashMap<String,TreeMap<String,ScriptExecution>> scriptExecutions = new HashMap<String,TreeMap<String,ScriptExecution>>();
	
	
	/**
	 * Loads the project from a file using deserialization. 
	 * @param filename
	 * @return
	 */
	public static AuditProject loadFromFile(String filename) {
		try {
			FileInputStream fos = new FileInputStream(filename);
			GZIPInputStream gz = new GZIPInputStream(fos);
			BufferedInputStream buf = new BufferedInputStream(gz);
			
			try {
				Input input = new Input(buf);
				AuditProject prj =  new Kryo().readObject(input, AuditProject.class);
				input.close();
				if (prj!=null)
					return prj;
			} 
			catch (Exception e) {
				e.printStackTrace();
				//for backward compatibility
			}
			
		} catch (Exception e) {
			e.printStackTrace();			
		}
		return null;
	}

	
	/**
	 * Saves the project contents to a file. Uses object serialization, Kryo library.
	 * @param filename
	 * @return
	 */
	public boolean saveToFile(String filename) {
		try {
			FileOutputStream fos = new FileOutputStream(filename);
			GZIPOutputStream gz = new GZIPOutputStream(fos);
			BufferedOutputStream buf = new BufferedOutputStream(gz);
			
							
			Output output = new Output(buf);
			new Kryo().writeObject(output, this);
			output.close();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public Object[] getFieldArray() {
		if (fieldList == null) {			
			fieldList = new HashMap<String,FieldDesc>();
			addFields(BaseDuObject.class.getFields(), fieldList);
			addFields(Task.class.getFields(), fieldList);
			addFields(Session.class.getFields(), fieldList);
			addFields(UProc.class.getFields(), fieldList);
			addFields(Resource.class.getFields(), fieldList);						
		}
		ArrayList<FieldDesc> l = new ArrayList<FieldDesc>();
		l.addAll(fieldList.values());		
		Collections.sort(l);
		return l.toArray();
	}

	private void addFields(Field fields[], HashMap<String,FieldDesc> list) {
		for (int i = 0; i < fields.length; i++) {				
			try {
				if (fields[i].isAnnotationPresent(ListViewField.class)) 
					if (!list.containsKey(fields[i].getName()))
						list.put(fields[i].getName(), new FieldDesc(fields[i].getName(), fields[i].getAnnotation(ListViewField.class).title(), false, fields[i].getAnnotation(ListViewField.class).editable()));
			} catch (Exception ex) {}				 			
		}
	}	
	public ArrayList<FieldDesc> getFieldArray(@SuppressWarnings("rawtypes") Class c) {		
		ArrayList<FieldDesc> l = new ArrayList<FieldDesc>();
		for (int i = 0; i < c.getFields().length; i++) {				
			try {
				if (c.getFields()[i].isAnnotationPresent(ListViewField.class)) 
					l.add(new FieldDesc(c.getFields()[i].getName(), c.getFields()[i].getAnnotation(ListViewField.class).title(), false, c.getFields()[i].getAnnotation(ListViewField.class).editable()));
			} catch (Exception ex) {}				 			
		}
		Collections.sort(l);
		return l;
	}
}
