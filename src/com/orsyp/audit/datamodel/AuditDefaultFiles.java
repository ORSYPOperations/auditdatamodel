package com.orsyp.audit.datamodel;

import java.io.File;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

public class AuditDefaultFiles {
	
	private static HashMap<String,String> files = null;
	
	public static String get(String file, String version) {
		if (file==null)
			return "";
		if (files==null)
			loadFiles();
		if (version.length()>3)
			version=version.substring(0, 3);
		String storedFilename = version.replace(".", "")+"_";		
		if (Arrays.asList(new String[] {"uxsetenv.bat","uxsetenv","unienv.bat","unienv","uxshutdown.bat","uxshutdown",
										"uxstartup.bat","uxstartup_gen.bat","uxstartup_gen","uxstartup","u_batch.bat","u_batch","security.txt"}).contains(file))
			storedFilename += file;
		if (file.startsWith("UNIVERSE_SECURITY"))
			storedFilename += "UNIVERSE_SECURITY";
				
		return files.get(storedFilename);
	}
	
	private static void loadFiles() {
		files = new HashMap<String,String>();
		
		File f = new File("files");
		for (String file : f.list()) {
			Scanner scan = null;
			try {
				scan =  new Scanner(new File("files/"+file)).useDelimiter("\\A");
				String content = scan.next();
				files.put(file,content);
			} catch(Exception e) {
				e.printStackTrace();
			} finally {
				scan.close();
			}
		}
	}

}
