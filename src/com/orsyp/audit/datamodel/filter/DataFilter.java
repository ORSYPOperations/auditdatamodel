package com.orsyp.audit.datamodel.filter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.orsyp.audit.datamodel.BaseDuObject;
import com.orsyp.audit.datamodel.annotations.AnnotatedObjUtils;
import com.orsyp.audit.datamodel.duobjects.Resource;
import com.orsyp.audit.datamodel.duobjects.Session;
import com.orsyp.audit.datamodel.duobjects.Task;
import com.orsyp.audit.datamodel.duobjects.UProc;


/**
 * A data filter contains a list of filtering criteria
 * @author rbr
 *
 */
public class DataFilter implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public String filterName = "";
	//a set of fliter groups
	public List<FilterGroup> groups = new ArrayList<FilterGroup>();
	
	public void removeField(FilterField data) {
		for (FilterGroup gr : groups) {
			if (gr.fields.contains(data)) {
				gr.fields.remove(data);
				if (gr.fields.size()==0)
					removeGroup(gr);
				return;
			}
		}
	}
	
	public void removeGroup(FilterGroup data) {
		groups.remove(data);
	}
	
	public void clear() {
		groups.clear();		
	}

	public boolean isAccceptedObject(Object o) {
		if (groups.size()==0)
			return true;
				
		//cycle groups until one is accepted
		for (int grpIdx = 0; grpIdx < groups.size(); grpIdx++) {
			boolean accept = true;
			
			FilterGroup g = groups.get(grpIdx);
			//cycle fields, all must be accepted
			for (int fIdx = 0; fIdx < g.fields.size(); fIdx++) {
				FilterField f = g.fields.get(fIdx);
				accept = fieldAccepted(o, f, g.objectType);
				//skip if a field check is not accepted
				if (!accept)
					break;
			}			
			
			if (accept)
				return true;
		}
		return false;
	}

	private boolean fieldAccepted(Object o, FilterField f, String objectType) {
		//generic model
		//du model
		if (o instanceof BaseDuObject) {
			if (f.dummyOnly)
				if (!((BaseDuObject)o).dummy)
					return false;
			if (f.warningsOnly)
				if (((BaseDuObject)o).getWarnings().size()==0)
					return false;
			if (objectType.equals("Task"))
				if (! (o instanceof Task))
					return false;
			if (objectType.equals("UProc")) 
				if (! (o instanceof UProc))
					return false;
			if (objectType.equals("Session"))
				if (! (o instanceof Session))
					return false;
			if (objectType.equals("Resource"))
				if (! (o instanceof Resource))
					return false;		
			
			//filter field value
			return checkOperator(AnnotatedObjUtils.getFieldValue(o, f.fieldId), f.operator, f.values);
		}
		return true;
	}

	private boolean checkOperator(String fieldValue, String operator, String[] values) {
		//"=", "Not =", "Contain", "Don't contain", "Start with", "End with", "Regular expression", "Similar", ">", "<", ">=", "<="
		boolean ret = false;
		for (int i = 0; i < values.length; i++) {
			if ((fieldValue==null) || (fieldValue.equals(""))) {
				if (operator.equals("Empty")) 
					ret = true;
				else
				if (operator.equals("Not empty")) 
					ret = false;
				else
				if (operator.equals("=")) 
					ret = values[i].equals("");
				else
				if (operator.equals("Not ="))
					ret = true;
				else
				if (operator.equals("Don't contain")) 
					ret = true;
			}
			else {
				if (operator.equals("Not empty")) 
					ret = true;
				else
				if (operator.equals("=")) 
					ret = fieldValue.equals(values[i]);
				else
				if (operator.equals("Not =")) 
					ret = !fieldValue.equals(values[i]);
				else
				if (operator.equals("Contain")) 
					ret = fieldValue.contains(values[i]);
				else
				if (operator.equals("Don't contain")) 
					ret = !fieldValue.contains(values[i]);
				else
				if (operator.equals("Start with")) 
					ret = fieldValue.startsWith(values[i]);
				else
				if (operator.equals("End with")) 
					ret = fieldValue.endsWith(values[i]);
				else
				if (operator.equals("Regular expression")) 
					ret = fieldValue.matches(values[i]);
				else
				if (operator.equals("Similar")) {
					DMSoundex dms = new DMSoundex();
					try {
						ret = dms.sencode(fieldValue).equals(dms.sencode(values[i]));
					} catch (Exception e) {
						ret = false;
					}
				}
				else
				if (operator.equals(">")) 
					ret = fieldValue.compareToIgnoreCase(values[i])==1;
				else
				if (operator.equals("<")) 
					ret = fieldValue.compareToIgnoreCase(values[i])==-1;
				else
				if (operator.equals(">=")) 
					ret = fieldValue.compareToIgnoreCase(values[i])!=-1;
				else
				if (operator.equals("=<")) 
					ret = fieldValue.compareToIgnoreCase(values[i])!=1;
			}
				
			if (ret)
				return true;
		}		
		return false;
	} 
}
