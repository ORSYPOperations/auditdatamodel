package com.orsyp.audit.datamodel.filter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Set of field filters
 * @author rbr
 *
 */
public class FilterGroup  implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public String objectType;
	public List<FilterField> fields = new ArrayList<FilterField>(); 

}
