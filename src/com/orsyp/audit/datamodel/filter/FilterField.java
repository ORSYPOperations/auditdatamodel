package com.orsyp.audit.datamodel.filter;

import java.io.Serializable;

/**
 * Field filter parameters
 * @author rbr
 *
 */
public class FilterField implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public String fieldId;
	public String fieldDisplayName;
	public String operator;
	public String[] values;
	public boolean warningsOnly;
	public boolean dummyOnly;
}

