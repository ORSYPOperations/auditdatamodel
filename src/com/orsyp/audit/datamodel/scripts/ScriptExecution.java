package com.orsyp.audit.datamodel.scripts;

import java.io.Serializable;
import java.util.TreeMap;
import java.util.Vector;

public class ScriptExecution implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public String name;	
	public String node;
	
	public boolean success = true;	
	public TreeMap<String,String> values = new TreeMap<String,String>(); 	
	public Vector<String[]> table = new Vector<String[]>();	
	public Vector<String> files = new Vector<String>();

}
