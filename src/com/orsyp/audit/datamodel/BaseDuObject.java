package com.orsyp.audit.datamodel;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.orsyp.audit.datamodel.annotations.ListViewField;
import com.orsyp.audit.datamodel.annotations.ListableObj;

/**
 * Common base class for $U data model objects.
 * The name field is the id.
 * @author rbr
 *
 */
public abstract class BaseDuObject implements Serializable, ListableObj {

	private static final long serialVersionUID = 1L;
	
	public static final String DUMMY_UPROC_APP = "DUMMY_OBJECTS";
	
	@ListViewField(title="Name")
	public String name;
	
	@ListViewField(title="Type")
	public String objectType;
	
	@ListViewField(title="Label")
	public String label;
	
	@ListViewField(title="Info")
	public String info;

	public boolean dummy = false;

	private List<AuditWarning> warnings = new ArrayList<AuditWarning>();	
						
	
	public List<AuditWarning> getWarnings() {
		return warnings;
	}

	public void addWarning(AuditWarning w) {
		warnings.add(w);
	}

	@Override
	public String getListString() {
		return name;
	}
	
}
