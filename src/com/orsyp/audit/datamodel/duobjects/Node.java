package com.orsyp.audit.datamodel.duobjects;

import java.io.Serializable;

public class Node  implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public String name;
	public String label;
	public String dev;
    public String prod; 
    public String nomoni; 
    public String dir;                                         
    public String adir;                                
    public String idir;                                
    public String sdir;                                
    public String xdir;                                
    public String ddir;                                    
    public String pdir; 
}