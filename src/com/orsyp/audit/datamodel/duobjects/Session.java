package com.orsyp.audit.datamodel.duobjects;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.orsyp.audit.datamodel.BaseDuObject;
import com.orsyp.audit.datamodel.annotations.ListViewField;
import com.orsyp.audit.datamodel.annotations.ListViewFieldType;
import com.orsyp.audit.datamodel.duobjects.UProc.UProcFunctionalType;
import com.orsyp.audit.datamodel.duobjects.helperobjects.SessionTreeElement;

/**
 * $U session implementation
 * @author rbr
 *
 */
public class Session extends BaseDuObject implements Serializable {

	private static final long serialVersionUID = 1L;
		
	@ListViewField(title="Session MU", type=ListViewFieldType.LISTABLEOBJ)
	public String mu;
	
	//session tree structure elements
	public List<SessionTreeElement> descriptions = new ArrayList<SessionTreeElement>();
	
	public String version;
	
	public boolean remote=false;
	public boolean hdp=false;
	
	//internal use
	public List<UProc> uprocs = new ArrayList<UProc>();
	public UProc header;
	public UProc trailer;
	public UProc trailerFail;
	
	public ArrayList<UProc> trailerOkList = new ArrayList<UProc>();
	public ArrayList<UProc> trailerKoList = new ArrayList<UProc>();
	
	
	public Session() {
		super();
		objectType = "SESSION";
	}

	/**
	 * Get the children of an uproc in the session tree 
	 * @param upr
	 * @param type
	 * @return
	 */
	public List<UProc> getChildren(UProc upr, UProcFunctionalType type) {
		List<UProc> list = new ArrayList<UProc>();
		for (SessionTreeElement st : descriptions) 
			if ((st.father!=null) && (st.father.equals(upr)))
				if (st.uproc.functionalType==type)
					list.add(st.uproc);
		return list;
	}
	
}
