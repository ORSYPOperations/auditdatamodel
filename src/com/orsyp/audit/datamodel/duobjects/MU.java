package com.orsyp.audit.datamodel.duobjects;

import java.io.Serializable;

import com.orsyp.audit.datamodel.BaseDuObject;

/**
 * $U MU implemnentation
 * @author rbr
 *
 */
public class MU extends BaseDuObject implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public static enum OSType {WINDOWS, UNIX, AS400};
	
	public String node;
	public OSType os;
	
	public String offset;

	public MU() {
	}
	
	public MU(String name, String node, OSType osType) {
		this.name = name;
		this.node = node;
		this.os = osType;
	}
}
