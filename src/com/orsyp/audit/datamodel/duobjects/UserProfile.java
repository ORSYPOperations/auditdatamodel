package com.orsyp.audit.datamodel.duobjects;

import java.io.Serializable;

public class UserProfile  implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public String name;
	public String label;	
}
