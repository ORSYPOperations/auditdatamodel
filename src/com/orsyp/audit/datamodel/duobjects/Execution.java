package com.orsyp.audit.datamodel.duobjects;

import java.io.Serializable;

public class Execution implements Serializable{
	private static final long serialVersionUID = 1L;
	
	public String name;
	public String status;
	public String start;
	public String end;
	public String aut;
	public String user;
	public String sessPro;
}
