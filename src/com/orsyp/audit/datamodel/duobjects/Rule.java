package com.orsyp.audit.datamodel.duobjects;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;

import com.orsyp.audit.datamodel.BaseDuObject;
import com.orsyp.audit.datamodel.duobjects.helperobjects.DayOfWeek;
import com.orsyp.audit.datamodel.duobjects.helperobjects.Period;
import com.orsyp.audit.datamodel.duobjects.helperobjects.Period.DayType;

/**
 * $U Rule implementation
 * @author rbr
 *
 */
public class Rule extends BaseDuObject implements Serializable, Comparable<Rule> {
	private static final long serialVersionUID = 1L;

	public enum Offset {NEXT,PREVIOUS,NONE};
	
	//period
	public Period period = new Period();
		
	//authorizations
	public ArrayList<DayOfWeek> daysofweek = new ArrayList<DayOfWeek>();
	public ArrayList<Integer> days = new ArrayList<Integer>();
	public ArrayList<Integer> months = new ArrayList<Integer>();
	
	//offset	
	public Offset offset = Offset.NONE;
	
	public boolean duDefaultRule = true;
	
	@SuppressWarnings("unused")
	private Rule() {}
	
	public Rule(String ruleName) {
		name = ruleName;
	}


	public boolean isAccepted(Calendar cal, DayType dayType) {
		int day = cal.get(Calendar.DAY_OF_MONTH);
		int month = cal.get(Calendar.MONTH) +1; //adjust, cal is 0 based
		int dayofweek = cal.get(Calendar.DAY_OF_WEEK)-1; //adjust, cal starts with sunday
		if (dayofweek<1)
			dayofweek=7;
		
		//check day, month, day of week filters
		
		if (days.size()>0) {
			if (!listContains(days,day))
				return false;
		}
		if (months.size()>0) {
			if (!listContains(months,month))
				return false;
			
		}
		if (daysofweek.size()>0) {
			for (DayOfWeek dow : daysofweek) {
				if (dow.dayofweek==dayofweek) {
					if (dayType==null)
						return dow.workday;
					else
						switch (dayType) {
							case WORKING: return dow.workday;
							case CLOSING: return dow.closing;
							case HOLIDAY: return dow.holiday;					
						}
				}
			}
			return false;
		}
		
		//then check period
		
		
		return true;
	}
	
	
	private boolean listContains(ArrayList<Integer> list, int value) {
		for (Integer i : list) 
			if (i.equals(value))
				return true;
		return false;
	}

	@Override
	public int compareTo(Rule r2) {		
		return name.compareToIgnoreCase(r2.name);
	}

}
