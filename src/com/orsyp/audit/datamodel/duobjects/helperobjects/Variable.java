package com.orsyp.audit.datamodel.duobjects.helperobjects;

import java.io.Serializable;

import com.orsyp.audit.datamodel.annotations.ListableObj;

/**
 * $U variable implementation
 * @author rbr
 *
 */
public class Variable implements Serializable, ListableObj {

	private static final long serialVersionUID = 1L;
	
	public String name;
	public String value;
	public String type = "T"; //Q,D,T  num,date,string  default T
	
	//default date format for variables
	public String format = "DD/MM/YYYY"; //for dates
	
	public String min;  //for numbers
	public String max;  //for numbers	

	public Variable(String name, String value) {
		super();
		this.name = name;
		this.value = value;
		this.type = "T";
	}
	
	@SuppressWarnings("unused")
	private Variable() {
		super();
	}

	@Override
	public String getListString() {
		return name+"="+value;
	}

}
