package com.orsyp.audit.datamodel.duobjects.helperobjects;

import java.io.Serializable;

import com.orsyp.audit.datamodel.annotations.ListableObj;



/**
 * $U uproc condition implementation
 * @author rbr
 *
 */
public class Condition implements Serializable, ListableObj  {

	private static final long serialVersionUID = 1L;
	
	//enums
	public static enum Type {DEPENDENCY,RESOURCE,NON_SIMULTANEITY};
	public static enum FileDepAttribute {EXIST,SIZE,DATEUNCHANGE};
	public static enum UProcStatus {COMPLETED,ABORTED,ABSENT};
	
	
	public Type type = Type.DEPENDENCY;
	private String conditionNumber;	
	public String info;	
	
	//uproc or resource id of the condition origin
	public String OriginId;
	
	//run on condition not present
	public boolean exclude = false;
	
	//not sure I need these fields for now
	//public String proposition; 
	//public boolean fatal;
	//public boolean reverse;
	
	//uproc condition fields
	//status of the uproc
	public UProcStatus uprocStatus = UProcStatus.COMPLETED;
	
	//resource condition fields
	//amount of the resource
	public FileDepAttribute attribute;
	public String amountOperator;
	public String amountValue;
	public int quota1;
	public int quota2;
	
	//fields for other condition types
	//...
	
	//additional checks 
	public ConditionChecks conditionChecks = new ConditionChecks();	; 

	@Override
	public String getListString() {
		if (type==Type.RESOURCE)
			return OriginId + "(RESOURCE)";
		if (type==Type.NON_SIMULTANEITY)
			return OriginId + "(EXCLUSION)";			
		if (uprocStatus!=null)
			return OriginId + "(" + uprocStatus + ")";			
		return OriginId;
	}
	
	public void setDateCheck(CheckType ch) {
		conditionChecks.dayCheck = ch;
		conditionChecks.monthCheck = ch;
		conditionChecks.yearCheck = ch;		
	}
	
	public void setConditionNumber(int num) {
		conditionNumber = String.format("=C%02d",  num);
	}
	
	public String getConditionNumber() {
		return conditionNumber;
	}

	
}