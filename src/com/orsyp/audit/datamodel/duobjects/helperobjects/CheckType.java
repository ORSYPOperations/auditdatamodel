package com.orsyp.audit.datamodel.duobjects.helperobjects;

import java.io.Serializable;

public enum CheckType implements Serializable {
	ANY,SAME,SAMERUN,VALUE,OFFSET
}
