package com.orsyp.audit.datamodel.duobjects.helperobjects;

import java.io.Serializable;

import com.orsyp.audit.datamodel.duobjects.UProc;

/**
 * $U session tree node implementation
 * @author rbr
 *
 */
public class SessionTreeElement implements Serializable {

	private static final long serialVersionUID = 1L;

	public SessionTreeElement parent;
	
	public String fatherId;
	public String id;
	
	public UProc uproc;
	public UProc father;
	public UProc bigBrother; //the first uproc on the same level (same father)
	public boolean success = true; //OK/KO
	
	public String mu; //optional
	public String hdpLeft; //??
	public String hdpRight; //??
	
}
