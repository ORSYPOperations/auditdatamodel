package com.orsyp.audit.datamodel.duobjects.helperobjects;

import java.io.Serializable;

import com.orsyp.audit.datamodel.duobjects.UProc;
import com.orsyp.audit.datamodel.duobjects.helperobjects.Condition.UProcStatus;


/**
 * $U uproc termination instructions implementation
 * @author rbr
 *
 */
public class TerminationInstruction implements Serializable {

	private static final long serialVersionUID = 1L;
	
	public ConditionChecks checks = new ConditionChecks();
	
	public UProcStatus status = UProcStatus.COMPLETED;	
	public UProc uproc;
	
	public TerminationInstruction() {
		checks.sessionCheck = CheckType.SAMERUN;
	}
	
	
}
