package com.orsyp.audit.datamodel.duobjects.helperobjects;

import java.io.Serializable;

/**
 * $U condition checks implementation
 * @author rbr
 *
 */
public class ConditionChecks implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public CheckType muCheck = CheckType.SAME;
	public String muValue;
	
	public CheckType sessionCheck = CheckType.ANY;
	public String sessionValue;
	
	public CheckType userCheck = CheckType.ANY;
	public String userValue;
	
	public CheckType dayCheck = CheckType.SAME;
	public String dayValue;
	
	public CheckType monthCheck = CheckType.SAME;
	public String monthValue;

	public CheckType yearCheck = CheckType.SAME;
	public String yearValue;

}
