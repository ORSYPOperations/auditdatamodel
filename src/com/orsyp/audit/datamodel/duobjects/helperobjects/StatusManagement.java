package com.orsyp.audit.datamodel.duobjects.helperobjects;

import java.io.Serializable;

/**
 * $U uproc exit status implementation
 * @author rbr
 *
 */
public class StatusManagement implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	public enum ManagedStatus {COMPLETED,ABORTED};
	
	public ManagedStatus status;
	public String operator;
	public String returnCode;
	public String searchedString;
	public String searchedFile;
	public String extendend;
	
	public String getAsString() {
		String ret = returnCode;
		if (operator!=null) { 
			if (!operator.startsWith(","))
				ret += ",";
			ret += operator;
		}
		return ret;
	}
	
}
