package com.orsyp.audit.datamodel.duobjects.helperobjects;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import com.orsyp.audit.datamodel.annotations.ListableObj;

/**
 * Time interval. Used for launch windows.
 * Can be set using a start time and a duration or an end time.
 * @author rbr
 *
 */
public class TimeInterval implements Serializable, ListableObj {

	private static final long serialVersionUID = 1L;
	
	private Calendar start;
	private Calendar end;

	//only getters, no setters, values set by factory methods
	public Calendar getStart() {
		return start;
	}
	public Calendar getEnd() {
		return end;
	}	

	//private constructor, create only with static factory methods
	private TimeInterval() {
		super();
		start = Calendar.getInstance();
		end = Calendar.getInstance();
	}
	
	public static TimeInterval createUsingStartDuration(String start, String duration) {
		TimeInterval ti = new TimeInterval();
		ti.start = getCalendarFromString(start);
		Calendar dur = getCalendarFromString(duration);
		ti.setDurationCal(dur);
		return ti;
	}
	
	public static TimeInterval createUsingStartDurationMinutes(String start, int minutes) {
		TimeInterval ti = new TimeInterval();
		ti.start = getCalendarFromString(start);
		ti.setDurationMinutes(minutes);
		return ti;
	}
	
	public static TimeInterval createUsingStartEndStrings(String start, String end) {
		TimeInterval ti = new TimeInterval();
		ti.start = getCalendarFromString(start);
		ti.end = getCalendarFromString(end);
		if (ti.start.after(ti.end))
			ti.end.add(Calendar.DAY_OF_MONTH, 1);
		return ti;
	}
	
	public static TimeInterval createUsingStartEndCalendars(Calendar start, Calendar end) {
		TimeInterval ti = new TimeInterval();
		ti.start.setTime(start.getTime());
		ti.end.setTime(end.getTime());
		return ti;
	}
	
	public static TimeInterval createDuplicate(TimeInterval ti) {
		TimeInterval t = new TimeInterval();
		t.start.setTime(ti.start.getTime());
		t.end.setTime(ti.end.getTime());
		return t;
	}		
		
	public int getDurationInMinutes() {
		int diff = (end.get(Calendar.MINUTE) + 60 * end.get(Calendar.HOUR_OF_DAY)) 
        		- (start.get(Calendar.MINUTE) + 60 * start.get(Calendar.HOUR_OF_DAY));
		if (diff<=0)
			return 24*60 + diff;
		return diff;
	}
	
	
	private void setDurationMinutes(int minutes) {
		if (start==null)
			return;
		//set duration
		end.setTime(start.getTime());
		end.add(Calendar.MINUTE, minutes);
	}
	
	private void setDurationCal(Calendar dur) {
		if (start==null)
			return;
		//set duration
		end.setTime(start.getTime());
		end.add(Calendar.MINUTE, dur.get(Calendar.MINUTE));
		end.add(Calendar.HOUR, dur.get(Calendar.HOUR_OF_DAY));
	}
	
	
	/**
	 * Check if the given time falls inside the interval
	 * @param time format: hh:mm
	 * @return
	 */
	public boolean includes(String time){		
		Calendar t = getCalendarFromString(time);
		if (t==null)
			return false;
		return (t.compareTo(start) >=0) && (t.compareTo(end)<=0); 
	}
	
	public boolean sameInterval(TimeInterval ti) {
		return 	(ti.start.getTimeInMillis()==start.getTimeInMillis()) && 
				(ti.end.getTimeInMillis()==end.getTimeInMillis());
	}

	@Override
	public String getListString() {		
		if (start==null)
			return "";
		if (end==null)
			return "";
		return new SimpleDateFormat("HH:mm").format(start.getTime()) + "-" + new SimpleDateFormat("HH:mm").format(end.getTime());
	}
	
	
	/**
	 * Converts a time string to a Calendar
	 * Accepted formats hh:mm h:mm hhmm hmm mm m
	 * 
	 * @param time
	 * @return
	 */
	private static Calendar getCalendarFromString(String time) {
		if (time==null)
			return null;		
		if ((time.length()==4) && time.contains(":"))
			time = "0"+time;
		while (time.length()<4)
			time = "0"+time;
			
		if (time.matches("\\d\\d\\d\\d") || time.matches("\\d\\d:\\d\\d")) {
			String hours = time.substring(0,2);
			String mins;
			if (time.contains(":"))				
				mins = time.substring(3,5);
			else
				mins = time.substring(2,4);
			Calendar t = Calendar.getInstance();
			t.setTimeInMillis(0);
			t.set(Calendar.HOUR_OF_DAY, Integer.parseInt(hours));
			t.set(Calendar.MINUTE, 		Integer.parseInt(mins));
			return t;
		}
		return null;
	}	
		

	public static boolean isInAnInterval(String time, List<TimeInterval> list){
		if (list.size()==0)
			return true;
		for (TimeInterval ti : list) 
			if (ti.includes(time))
				return true;			
		return false;
	}
	
	public static TimeInterval getEnclosingInterval(String time, List<TimeInterval> list){
		for (TimeInterval ti : list) 
			if (ti.includes(time))
				return ti;			
		return null;
	}
	
	/**
	 * Insert a new launch window in the list
	 * 
	 * @param startTime
	 * @param list
	 * @param defaultEnd
	 */
	public static void insertIntoLaunchList(String startTime, List<TimeInterval> list, String defaultEnd) {
		
		Calendar start = getCalendarFromString(startTime);
		
		//the list is ordered, find the position
		int idx=0;
		for (TimeInterval ti : list) {
			if (ti.start.after(start)) 
				break;
			idx++;
		}
		
		//default window end
		Calendar end = getCalendarFromString(defaultEnd);
		//if there is another window after this
		if (idx<list.size()-1) {
			//use next window's start -1 minute as window end
			TimeInterval nextTi = list.get(idx);
			end.setTimeInMillis(nextTi.start.getTimeInMillis());
			end.add(Calendar.MINUTE, -1);
		}
		
		//create the new window and insert it in the list in the right position				
		
		//TimeInterval win = new TimeInterval(start, end);
		TimeInterval win = createUsingStartEndCalendars(start, end);
		list.add(idx, win);
			
		//shrink previous window if present
		if (idx>0) {
			//move the end of the time window to avoid overlapping
			TimeInterval prevTi = list.get(idx-1);
			Calendar prevEnd = Calendar.getInstance();
			prevEnd.setTimeInMillis(start.getTimeInMillis());
			prevEnd.add(Calendar.MINUTE, -1);
			prevTi.end = prevEnd; 
		}
	}
	
	
	public static void insertIntoLaunchListWithDuration(String startTime, List<TimeInterval> list, int minutes) {
		Calendar start = getCalendarFromString(startTime);
		
		//the list is ordered, find the position
		int idx=0;
		for (TimeInterval ti : list) {
			if (ti.start.after(start)) 
				break;
			idx++;
		}
		
		//default window end
		Calendar end = Calendar.getInstance();
		end.setTime(start.getTime());
		end.add(Calendar.MINUTE, minutes);
		//if there is another window after this
		if (idx<list.size()-1) {
			//use next window's start -1 minute as window end
			TimeInterval nextTi = list.get(idx+1);
			if (nextTi.getStart().before(end)) {
				end.setTimeInMillis(nextTi.start.getTimeInMillis());
				end.add(Calendar.MINUTE, -1);
			}
		}
		
		//create the new window and insert it in the list in the right position				
		
		//TimeInterval win = new TimeInterval(start, end);
		TimeInterval win = createUsingStartEndCalendars(start, end);
		list.add(idx, win);
			
		//shrink previous window if present
		if (idx>0) {
			//move the end of the time window to avoid overlapping
			TimeInterval prevTi = list.get(idx-1);
			Calendar prevEnd = Calendar.getInstance();
			if (prevEnd.after(start)) {
				prevEnd.setTimeInMillis(start.getTimeInMillis());
				prevEnd.add(Calendar.MINUTE, -1);
				prevTi.end = prevEnd; 
			}
		}
	}
	
}
