package com.orsyp.audit.datamodel.duobjects.helperobjects;

import java.io.Serializable;

public class Period implements Serializable{
	private static final long serialVersionUID = 1L;
	
	public enum DayType {WORKING,CLOSING,HOLIDAY,
						CALENDAR,
						MONDAY,TUESDAY,WEDNESDAY,THURSDAY,FRIDAY,SATURDAY,SUNDAY};
	public enum PeriodType {DAY,MONTH,WEEK,QUARTER,YEAR,
							WORKING,CLOSING,HOLIDAY,
							MONDAY,TUESDAY,WEDNESDAY,THURSDAY,FRIDAY,SATURDAY,SUNDAY,
							FISCALMONTH,FISCALQUARTER,FISCALSEMESTER,FISCALYEAR};
	public enum PositionType {FORWARD,BACKWARD};	
	
	public PeriodType period = PeriodType.DAY;
	public int amount = 1;
			
	public PositionType positionDirection = PositionType.FORWARD;
	public int positionNumber = 1;
	public DayType positionDayType = DayType.CALENDAR;

}
