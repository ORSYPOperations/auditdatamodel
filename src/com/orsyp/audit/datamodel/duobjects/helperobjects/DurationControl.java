package com.orsyp.audit.datamodel.duobjects.helperobjects;

import java.io.Serializable;

/**
 * $U uproc duration controls implementation
 * @author rbr
 *
 */
public class DurationControl implements Serializable {

	private static final long serialVersionUID = 1L;
	
	public static String SESSION_VAR = "<$U_SESSION_NAME>";
	public static String UPROC_VAR = "<$U_UPROC_NAME>";

	public int hours = 0;
	public int minutes = 0;
	public int seconds = 0;
	public int percent = 0;
	
	public String action = "C"; //K,F,C,A,R
	
	public String script = "TEMP";
	
	public String toHHHMMSSorPercent() {
		if (percent>0)
			return ""+percent;
		return String.format("%03d%02d%02d", hours, minutes, seconds);
	}

	
}
