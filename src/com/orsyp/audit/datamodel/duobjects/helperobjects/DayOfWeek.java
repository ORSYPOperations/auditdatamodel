package com.orsyp.audit.datamodel.duobjects.helperobjects;

import java.io.Serializable;

public class DayOfWeek implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public int dayofweek;
	public boolean workday;
	public boolean closing;
	public boolean holiday;
	
	@SuppressWarnings("unused")
	private DayOfWeek (){}
	
	public DayOfWeek (int dayofweek, boolean workday, boolean closing, boolean holyday) {
		this.dayofweek = dayofweek;
		this.workday = workday;
		this.closing = closing;
		this.holiday = holyday;
	}

}
