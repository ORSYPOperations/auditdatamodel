package com.orsyp.audit.datamodel.duobjects.lightobjects;

import java.io.Serializable;

import com.orsyp.audit.datamodel.BaseLtObject;

public class UProcLt extends BaseLtObject implements Serializable {
	private static final long serialVersionUID = 1L;

	public UProcLt() {
		objectType = "UPROC";
	}
	
	public String version;
}