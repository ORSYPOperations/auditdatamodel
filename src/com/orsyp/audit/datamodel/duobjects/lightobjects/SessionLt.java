package com.orsyp.audit.datamodel.duobjects.lightobjects;

import java.io.Serializable;

import com.orsyp.audit.datamodel.BaseLtObject;

public class SessionLt extends BaseLtObject implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public SessionLt() {
		objectType = "SESSION";
	}

	public String version;
}
