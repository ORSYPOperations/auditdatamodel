package com.orsyp.audit.datamodel.duobjects.lightobjects;

import java.io.Serializable;

import com.orsyp.audit.datamodel.BaseLtObject;

public class TaskLt extends BaseLtObject implements Serializable {
	private static final long serialVersionUID = 1L;

	public TaskLt() {
		objectType = "TASK";
	}
	
	public String session;
	public String sessionVer;
	
	public String uproc;
	public String uprocVer;
	
	public String mu;
	
	public String nature;
	
	public String startDate;
	public String startAt;
	
	public String endDate;
	public String endAt;
	
	public String processingDate;
	
	public String taskType;
	
	public String status;
	
	public String control;
	
	public String user;	
}
