package com.orsyp.audit.datamodel.duobjects;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.orsyp.audit.datamodel.BaseDuObject;
import com.orsyp.audit.datamodel.annotations.ListViewField;
import com.orsyp.audit.datamodel.annotations.ListViewFieldType;
import com.orsyp.audit.datamodel.duobjects.helperobjects.TimeInterval;
import com.orsyp.audit.datamodel.duobjects.helperobjects.Variable;

/**
 * $U task implementation.
 * @author rbr
 *
 */
public class Task extends BaseDuObject implements Serializable {

	private static final long serialVersionUID = 1L;
	
	public enum Type {SCHEDULED,OPTIONAL,PROVOKED,CYCLICAL};
	public enum Activation {ENABLED,DISABLED,SIMULATED};

	@ListViewField(title="Task Session")
	public String sessionId;
	@ListViewField(title="Task UProc")
	public String uprocId;	
	
	public String vses;
	public String vupr;	
	
	@ListViewField(title="Task User")
	public String user;
	@ListViewField(title="Task MU")
	public String mu;
	
	@ListViewField(type=ListViewFieldType.LISTABLEOBJ_LIST, title="Rules")
	public List<Rule> rules = new ArrayList<Rule>();
	@ListViewField(type=ListViewFieldType.LISTABLEOBJ_LIST, title="Excl. Rules")
	public List<Rule> excludedRules = new ArrayList<Rule>();
	
	@ListViewField(type=ListViewFieldType.LISTABLEOBJ_LIST, title="Task Variables")
	public List<Variable> variables = new ArrayList<Variable>();
	
	//launch info
	@ListViewField(title="Launch windows", type=ListViewFieldType.LISTABLEOBJ_LIST)
	public List<TimeInterval> launchWindows = new ArrayList<TimeInterval>();
	
	public Type type = Type.SCHEDULED;
	public String priority;	
		
	//internal use
	//this is used for provoked and optional tasks only
	public String parentTask = null;
	
	//task activation, default ENABLED
	public Activation activation = Activation.ENABLED;
	public boolean outOfSession = false;	
	public boolean shiftedRules = false;
	
	
	public boolean parallel = false;
	public boolean mult = false;
	public boolean model = false;

	public boolean force = false;
	public boolean restart = false;
	public boolean central = false;
	public boolean history = false;
	public boolean offsetPdate = false;

	public boolean autorel = false;
		
	public Task() {
		super();
		objectType = "TASK";
	}
	
}
