package com.orsyp.audit.datamodel.duobjects;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.orsyp.audit.datamodel.BaseDuObject;
import com.orsyp.audit.datamodel.annotations.ListViewField;
import com.orsyp.audit.datamodel.annotations.ListViewFieldType;
import com.orsyp.audit.datamodel.duobjects.helperobjects.Condition;
import com.orsyp.audit.datamodel.duobjects.helperobjects.DurationControl;
import com.orsyp.audit.datamodel.duobjects.helperobjects.StatusManagement;
import com.orsyp.audit.datamodel.duobjects.helperobjects.TerminationInstruction;
import com.orsyp.audit.datamodel.duobjects.helperobjects.Variable;
import com.orsyp.audit.datamodel.utils.RegExMatcher;

/**
 * $U uproc implementation
 * @author rbr
 *
 */
public class UProc extends BaseDuObject implements Serializable  {
	
	public static enum UProcType {CL_INT,CL_EXT,CMD,FTP,JMS_SEND,SAP_IPACK,SAP_XBP2,SAP_XBP2_STEP,SAP_PCHAIN,EJB};	

	private static final long serialVersionUID = 1L;
	
	@ListViewField(title="Formula")
	public String formula;
	@ListViewField(title="Functional period")
	public String fPeriod;
	@ListViewField(title="Severity")
	public String severity;
	
	
	@ListViewField(title="App")
	public String applicationId;
	
	@ListViewField(title="Version")
	public String version; 
	
	public String fullType;
	
	@ListViewField(title="Domain")
	public String domain;
	@ListViewField(title="Class")
	public String classId;
	@ListViewField(title="UProc MU")
	public String mu;	
	
	@ListViewField(type=ListViewFieldType.LISTABLEOBJ_LIST,title="UProc Variables")
	public List<Variable> variables = new ArrayList<Variable>();
	//@ListViewField(type=ListViewFieldType.STRING_LIST, title="Incomp. classes")
	//public List<String> incompatibleClasses = new ArrayList<String>();
	@ListViewField(title="Conditions", type=ListViewFieldType.LISTABLEOBJ_LIST)
	public List<Condition> conditions = new ArrayList<Condition>();
	
	@ListViewField(title="Mail to", type=ListViewFieldType.STRING_LIST)
	public List<String> mailTo = new ArrayList<String>();
	@ListViewField(title="Mail params", type=ListViewFieldType.STRING_LIST)
	public List<String> mailParams = new ArrayList<String>();	
	
	@ListViewField(title="Command")
	public String command;
	@ListViewField(title="Work dir.")
	public String workdir;
	@ListViewField(title="UProc type", type=ListViewFieldType.TYPE)
	public UProcType commandType;
	@ListViewField(title="OS")
	public String os;
	@ListViewField(title="Cmd params", type=ListViewFieldType.STRING_LIST)
	public List<String> commandParams = new ArrayList<String>();
		
	public String eventMemorization; //one,none,all values:o,n,a
	public String eventNumber;

	public StatusManagement statusManagement;
	
	@ListViewField(title="Script template")
	public String template;
	
	//v6 stuff
	
	//retries
	public int maxRetries = -1; //-1 = ignore, >=0 set value
	public int retryWaitMinutes = 1;
	
	//duration control
	public DurationControl maxDuration;
	public DurationControl minDuration;
	public DurationControl maxWaitDuration;
	
	public List<TerminationInstruction> terminationInstructions = new ArrayList<TerminationInstruction>();
	
	
	//for internal gui use
	public static enum UProcFunctionalType {STANDARD,TECHNICAL,HEADER,TRAILER_OK,TRAILER_KO};
	public UProcFunctionalType functionalType = UProcFunctionalType.STANDARD;
	
	
	public UProc() {
		super();
		objectType = "UPROC";
	}	
	
	public boolean isFormulaValid() {
		if (formula==null)
			return conditions.size()==0;
		
		if (formula.length()==0 && conditions.size()>0)
			return false;
		
		if (formula.length()>0) {
			if ((RegExMatcher.getMatches(formula, "ID\\d+").size()>0))
				return false;
			
			if (conditions.size()==0)
				if (RegExMatcher.getMatches(formula, "C\\d\\d").size()>0)			
					return false;
		}
		
		List<String> terms = RegExMatcher.getMatches(formula, "=C\\d\\d");
		if (terms.size()>99)
			return false;
		
		if (terms.size()!=conditions.size())
			return false;
		
		for (Condition c : conditions) 
			if (!terms.contains(c.getConditionNumber()))
				return false;
		
		return true;
	}
	
	public String getVariable(String varName) {
		for (Variable v : variables) 
			if (varName.equals(v.name))
				return v.value;
		return null;
	}
}
