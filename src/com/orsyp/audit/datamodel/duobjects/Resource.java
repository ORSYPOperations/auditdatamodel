package com.orsyp.audit.datamodel.duobjects;

import java.io.Serializable;

import com.orsyp.audit.datamodel.BaseDuObject;
import com.orsyp.audit.datamodel.annotations.ListViewField;
import com.orsyp.audit.datamodel.annotations.ListViewFieldType;

/**
 * $U resource implementation
 * @author rbr
 *
 */
public class Resource extends BaseDuObject implements Serializable {
	
	public static enum Type {FILE,LOGICAL,SYSTEM,SCRIPT,SAP_EVENT,SAP_JOB}; 

	private static final long serialVersionUID = 1L;
	
	@ListViewField(title="Resource type", type=ListViewFieldType.TYPE)
	public Type resourceType;
	
	@ListViewField(title="Scan frequency")
	public String scanFrequency;
	
	@ListViewField(title="File name")
	public String file;
	
	@ListViewField(title="Quota 1")
	public String quota1;
	
	@ListViewField(title="Quota 2")
	public String quota2;
	
	
	public boolean lock = false;
	public boolean clear = true;

	public boolean reserved = false;

	public String fullType;
		

	public Resource() {
		super();
		objectType = "RESOURCE";
	}	

}
