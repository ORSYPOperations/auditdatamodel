package com.orsyp.audit.datamodel.duobjects;

import java.io.Serializable;

import com.orsyp.audit.datamodel.BaseDuObject;

/**
 * $U user implementation
 */
public class User extends BaseDuObject implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public MU.OSType os = MU.OSType.UNIX;
	
	public String profile;
	public String code;
	public String type;	

	@SuppressWarnings("unused")
	private User() {}
	
	public User(String userName) {
		super();
		name = userName;
	}
	
}
