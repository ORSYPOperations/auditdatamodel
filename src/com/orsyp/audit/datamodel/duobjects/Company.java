package com.orsyp.audit.datamodel.duobjects;

import java.io.Serializable;

public class Company implements Serializable {
	private static final long serialVersionUID = 1L;
	
	public String name;
	public String label;
	public String master;
	public String dir;
	public boolean locked;
	
}
